#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
****************************************************************************
monografiebot.py
                      -------------------
begin                : 2016-04-26
copyright            : (C) 2016 by Salvatore Larosa
email                : lrssvtml (at) gmail (dot) com
****************************************************************************
"""

import logging
import ConfigParser
import sys
import psycopg2
import psycopg2.extras
import urllib2


config = ConfigParser.ConfigParser()
config.read(('settings.ini'))
token = config.get('main', 'token')
libs_path = config.get('path', 'libs_path')

db_name = config.get('db_connection', 'db_name')
db_user = config.get('db_connection', 'db_user')
db_passwd = config.get('db_connection', 'db_passwd')

sys.path.append(libs_path)

# from telegram import Updater, ParseMode, ReplyKeyboardMarkup, ReplyKeyboardHide
from telegram import ParseMode
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, RegexHandler
from bs4 import BeautifulSoup

# Enable Logging
logging.basicConfig(
        filename='error_monografiebot.log',
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger("monografiebot")
last_chat_id = 0

try:
    conn=psycopg2.connect("dbname=" + db_name + " user=" + db_user + " password=" + db_passwd)
except:
    print "No connection"

cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

def get_codcat_comune(com):
    comune = com.lower()
    try:
        sql = "SELECT * FROM elenco_comuni_italiani WHERE LOWER(comune)='{0}'".format(comune)
        cur.execute(sql)
    except:
        return "I can't SELECT from elenco-comuni-italiani"

    rows = cur.fetchall()
    if rows:
        return rows[0]['codice_cat'], rows[0]['provincia'], comune.upper()

    return None, None, None


def get_content_from_ae(query_string):
    url = 'http://www1.agenziaentrate.gov.it/servizi/Monografie/'
    page = url + 'risultato.php?{0}'.format(query_string)

    content = urllib2.urlopen(page.replace(' ', '%20')).read()
    soup = BeautifulSoup(content, "html.parser")
    divs = soup.findAll('div', attrs={'class' : 'container_sx_body'})

    a_tags = divs[0].find_all('a')

    links = []
    for a in a_tags:
        links.append(a['href'])

    return links, url


def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=u"Ciao, inviami il nome del comune ed il numero di foglio catastale per il quale "
                         u"vuoi scaricare le schede monografiche dei punti fiduciali. "
                         u"Comune e foglio devono essere separati dalla virgola. "
                         u"/help")
    bot.setWebhook()


def send_monografie(bot, update):
    text = update.message.text
    comune, foglio = text.split(',')

    codcat, provincia, comune_up = get_codcat_comune(comune.strip())

    if codcat:
        querystring = 'provincia={0}&comune={1}&co={2}&foglio={3}'.format(provincia.upper(), comune_up,
                                                                          codcat, foglio.strip())
    else:
        bot.sendMessage(chat_id=update.message.chat_id, text="Spiacente, nessun risultato.",
                        parse_mode=ParseMode.MARKDOWN)
        pass

    if text is not None:
        links, url = get_content_from_ae(querystring)
        if links:
            txt = u'*Codice catastale:* ' + codcat + '\n'
            if len(links) > 1:
                txt += u'*' + str(len(links)) + ' Schede Monografiche trovate:*\n\n'
            elif len(links) == 1:
                txt += u'*' + str(len(links)) + ' Scheda Monografica trovata:*\n\n'

            cnt = 1
            for link in links:
                title = link.split('&')[3].split('=')[1]
                txt += u'Monografia ' + str(cnt) + ': [' + title + '](' + url + link + ')\n\n'

                cnt += 1
            bot.sendMessage(chat_id=update.message.chat_id, text=txt, parse_mode=ParseMode.MARKDOWN)
        else:
            bot.sendMessage(chat_id=update.message.chat_id, text="Spiacente, nessun risultato.",
                            parse_mode=ParseMode.MARKDOWN)
    else:
        pass


def help(bot, update):
    """ Help message """

    text = u"_I PDF delle schede monografiche dei punti fiduciali possono essere " \
           u"ricercate digitando il nome del Comune ed il foglio catastale._ " \
           u"_Per eseguire la ricerca per il Comune di_ *Rende* _e il foglio_ *12* _digitare:_ *rende,12*. " \
           u"_Il nome del Comune può essere scirtto sia in maiuscolo che minuscolo._"
    bot.sendMessage(chat_id=update.message.chat_id, text=text, parse_mode=ParseMode.MARKDOWN)


def any_message(bot, update):
    """ Print to console """

    # Save last chat_id to use in reply handler
    global last_chat_id
    last_chat_id = update.message.chat_id

    logger.info("New message\nFrom: %s\nchat_id: %d\nText: %s" %
                  (update.message.from_user,
                   update.message.chat_id,
                   update.message.text))


def error(bot, update, error):
    """ Print error to console """
    logger.warn('Update %s caused error %s' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token=token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.addHandler(CommandHandler("start", start))
    dp.addHandler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.addHandler(MessageHandler([Filters.text], send_monografie))

    # log all errors
    dp.addErrorHandler(error)
    dp.addHandler(RegexHandler('.*', any_message), group=1)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()